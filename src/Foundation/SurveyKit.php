<?php


namespace Mediapress\Survey\Foundation;

use http\Exception;
use Mediapress\Survey\Models\Survey;
use Mediapress\Survey\Models\SurveyQuestion;

class SurveyKit
{
    public $key;
    public $websiteId;
    public $cookieId;
    public $startDate;
    public $finishDate;
    public $resultTime;

    public $showStatus;

    public $authCheck;
    public $userExists;
    public $ipExist;
    public $cookieExist;

    public $survey;
    public $questions;


    /**
     * @param string $key
     * @return $this
     */
    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return $this
     */
    public function init(): self
    {
        $this->setWebsiteId();
        $this->setSurvey();

        if (is_null($this->survey)) {
            return $this;
        }
        $this->setShowStatus();
        $this->setSettings();
        $this->setConditions();
        $this->setQuestions();

        return $this;
    }

    public function setWebsiteId(): void
    {
        $mediapress = mediapress();
        $this->websiteId = $mediapress->website->id;
    }

    private function setSurvey(): void
    {
        $survey = Survey::where('key', $this->key)
            ->where(function ($q) {
                $q->where('status', 1)
                    ->orWhere(function ($qu) {
                        $qu->where('status', 3)
                            ->where('start_date', '<=', date('Y-m-d'));
                    });
            })
            ->where(function ($q) {
                $q->whereNull('finish_date')
                    ->orWhere('finish_date', '>', date('Y-m-d'));
            })
            ->where('website_id', $this->websiteId)
            ->first();

        $this->survey = $survey;
    }

    /**
     * @throws \Exception
     */
    private function setQuestions(): void
    {
        if (is_null($this->survey)) {

            surveyLog(null, [
                'function' => 'setQuestions',
                'file' => __CLASS__,
                'line' => __LINE__,
                'message' => "Survey should be set before questions or there is no any survey !!"
            ]);

            throw new \Exception("Survey should be set before questions or there is no any survey !!");
        }

        $this->questions = $this->survey->questions()
            ->where('status', 1)
            ->orderBy('order')
            ->whereHas('detail')
            ->select('id', 'key', 'status', 'answer_type', 'cvar', 'ctex', 'cint')
            ->with([
                'detail',
                'children' => function ($q) {
                    $q->where('status', 1)
                        ->whereHas('detail')
                        ->orderBy('order')
                        ->with('detail')
                        ->select('id', 'survey_question_id', 'key', 'status', 'answer_type', 'cvar', 'ctex', 'cint');
                },
                'answers' => function ($q) {
                    $q->where('status', 1)
                        ->orderBy('order')
                        ->with('detail');
                }
            ])
            ->get();
    }

    private function setSettings(): void
    {
        $this->cookieId = $this->survey->cookie_id;
        $this->startDate = $this->survey->start_date;
        $this->finishDate = $this->survey->finish_date;
        $this->resultTime = $this->survey->result_time;
    }

    private function setConditions(): void
    {
        $this->authCheck = $this->survey->voting_condition == 2;

        $userId = auth()->check() ? auth()->id() : null;
        $this->userExists = $userId && $this->survey->results()->where("user_id", $userId)->count();

        $ip = getClientIp();
        $this->ipExist = $this->survey->results()->where("ip", $ip)->count();

        $cookieArray = getClientCookie();
        $this->cookieExist = in_array($this->survey->cookie_id, $cookieArray);
    }

    private function setShowStatus()
    {
        $this->showStatus = true;
        if ($this->survey->voting_condition == 2 && !auth()->check()) {
            $this->showStatus = false;
        }

        $resultIps = $this->survey->results->pluck('ip')->toArray();
        $ip = getClientIp();
        if ($this->survey->same_ip == 1 && in_array($ip, $resultIps)) {
            $this->showStatus = false;
        }

        $cookieArray = getClientCookie();
        if ($this->survey->same_cookie == 1 && in_array($this->survey->cookie_id, $cookieArray)) {
            $this->showStatus = false;
        }
    }

    public function howTheAttenderIdentified(){

    }
}
