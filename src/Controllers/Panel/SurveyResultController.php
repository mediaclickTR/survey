<?php

namespace Mediapress\Survey\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Survey\Models\Survey;

class SurveyResultController extends Controller
{

    public function index($surveyId)
    {
        if(! activeUserCan([
            "survey.survey.index",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($surveyId);
        $questions = $survey->questions()->where('answer_type', '<>', 3)->get();

        $results = [];

        foreach ($questions as $question) {
            $results[$question->id] = [
                'id' => $question->id,
                'name' => strip_tags($question->detail->name),
            ];
            $temp = [];
            $temp2 = [];
            foreach ($question->answers as $answer) {

                $temp2[1][] = $answer->result_count;
                $temp2[2][] = "'" . strip_tags($answer->detail->name) . "'";

                $temp[$answer->id] = [
                    'id' => $answer->id,
                    'name' => strip_tags($answer->detail->name),
                    'image' => image($answer->image)->resize(['w' => 200, 'h' => 200])->url,
                    'rate' => $answer->result_rate,
                    'count' => $answer->result_count,
                ];
            }
            $results[$question->id]['answer_count'] = implode(', ', $temp2[1]);
            $results[$question->id]['answer_names'] = implode(', ', $temp2[2]);
            $results[$question->id]['answers'] = $temp;
        }


        $inputQuestions = $survey->questions()->where('answer_type', 3)->get();

        $inputResults = [];

        foreach ($inputQuestions as $question) {

            $inputResults[$question->id] = [
                'id' => $question->id,
                'name' => strip_tags($question->detail->name),
            ];
            $temp = [];
            foreach ($question->results as $result) {

                $temp[$result->id] = [
                    'id' => $result->id,
                    'name' => strip_tags($result->custom_answer),
                    'ip' => $result->result->ip
                ];
            }
            $inputResults[$question->id]['answers'] = $temp;

        }

        return view('SurveyView::result.index', compact('survey', 'results', 'inputResults'));
    }
}
