<?php

namespace Mediapress\Survey\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Survey\Models\Survey;
use Mediapress\Survey\Models\SurveyAnswer;
use Mediapress\Survey\Models\SurveyAnswerDetail;
use Mediapress\Survey\Models\SurveyQuestion;
use Mediapress\Survey\Models\SurveyQuestionDetail;

class SurveyAnswerController extends Controller
{
    private $rules = [
        'order' => 'required',
        'status' => 'required',
    ];

    public function index($surveyId, $questionId)
    {
        if(! activeUserCan([
            "survey.survey.index",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($surveyId);
        $question = SurveyQuestion::find($questionId);

        if (is_null($survey) || is_null($question)) {
            abort(404);
        }

        $answers = SurveyAnswer::where('survey_id', $surveyId)
            ->where('survey_question_id', $questionId)
            ->orderBy('order')
            ->get();

        return view('SurveyView::answer.index', compact('survey', 'question', 'answers'));
    }

    public function create($surveyId, $questionId)
    {
        if(! activeUserCan([
            "survey.survey.update",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($surveyId);
        $question = SurveyQuestion::find($questionId);

        if (is_null($survey) || is_null($question)) {
            abort(404);
        }

        $countryGroups = session('panel.website')->countryGroups;

        return view('SurveyView::answer.create', compact('survey', 'question','countryGroups'));
    }

    public function store($surveyId, $questionId, Request $request)
    {

        $data = $request->except('_token');

        $labels = [
            'order' => __('SurveyPanel::create.answer.label.order'),
            'status' => __('SurveyPanel::create.answer.label.status'),
        ];


        $this->validate($request, $this->rules, [], $labels);

        $data['survey_id'] = $surveyId;
        $data['survey_question_id'] = $questionId;
        $data['admin_id'] = auth('admin')->id();

        $languages = $data['languages'] ?? [];
        unset($data['languages']);

        $data['image'] = isset($data['image']) ? str_replace(['[', ']'], '', $data['image']) : null;


        $answer = SurveyAnswer::firstOrCreate($data);

        $answer->update(['key' => md5($answer->id)]);

        foreach ($languages as $countryGroupId => $langs) {
            foreach ($langs as $languageId => $values) {
                if($values['name']) {
                    if($values['name']) {
                        SurveyAnswerDetail::firstOrCreate(
                            [
                                'survey_answer_id' => $answer->id,
                                'country_group_id' => $countryGroupId,
                                'language_id' => $languageId,
                            ],
                            $values
                        );
                    }
                }
            }
        }

        return redirect()->route('Survey.answer.index', ['surveyId' => $surveyId, 'questionId' => $questionId]);
    }

    public function edit($surveyId, $questionId,  $id)
    {
        if(! activeUserCan([
            "survey.survey.update",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($surveyId);
        $question = SurveyQuestion::find($questionId);
        $answer = SurveyAnswer::find($id);
        $countryGroups = session('panel.website')->countryGroups;

        return view('SurveyView::answer.edit', compact('survey', 'question', 'answer', 'countryGroups'));
    }

    public function update($surveyId, $questionId, $id, Request $request)
    {

        $data = $request->except('_token');

        $survey = Survey::find($surveyId);
        $question = SurveyQuestion::find($questionId);
        $answer = SurveyAnswer::find($id);

        if (is_null($survey)) {
            return redirect()->back()->withError(['message' => "Anket bulunamadı"]);
        }
        if (is_null($question)) {
            return redirect()->back()->withError(['message' => "Soru bulunamadı"]);
        }
        if (is_null($answer)) {
            return redirect()->back()->withError(['message' => "Cevap bulunamadı"]);
        }

        $labels = [
            'order' => __('SurveyPanel::create.answer.label.order'),
            'status' => __('SurveyPanel::create.answer.label.status'),
        ];


        $this->validate($request, $this->rules, [], $labels);

        $languages = $data['languages'] ?? [];
        unset($data['languages']);

        $data['image'] = isset($data['image']) ? str_replace(['[', ']'], '', $data['image']) : null;

        $answer->update($data);

        foreach ($languages as $countryGroupId => $langs) {
            foreach ($langs as $languageId => $values) {
                if($values['name']) {
                    SurveyAnswerDetail::firstOrCreate(
                        [
                            'survey_answer_id' => $answer->id,
                            'country_group_id' => $countryGroupId,
                            'language_id' => $languageId,
                        ],
                        $values
                    );
                }
            }
        }

        return redirect()->route('Survey.answer.index', ['surveyId' => $surveyId, 'questionId' => $questionId]);
    }

    public function delete($surveyId, $questionId, $id)
    {
        if(! activeUserCan([
            "survey.survey.delete",
        ])){
            return rejectResponse();
        }
        $answer = SurveyAnswer::find($id);

        if ($answer) {
            $answer->delete();

            return redirect()->route('Survey.answer.index', ['surveyId' => $surveyId, 'questionId' => $questionId]);
        } else {
            return redirect()->back()->withError(['message' => "Cevap bulunamadı"]);
        }
    }
}
