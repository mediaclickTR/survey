<?php

namespace Mediapress\Survey\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Survey\Models\Survey;

class SurveyController extends Controller
{
    private $rules = [
        'name' => 'required',
        'status' => 'required',
        'start_date' => 'required_if:status, 3|nullable',
        'result_time' => 'required',
        'voting_condition' => 'required',
        'same_ip' => 'required',
        'same_cookie' => 'required',
    ];


    public function index()
    {
        if(! activeUserCan([
            "survey.survey.index",
        ])){
            return rejectResponse();
        }

        $surveys = Survey::all();

        return view('SurveyView::survey.index', compact('surveys'));
    }

    public function create()
    {
        if(! activeUserCan([
            "survey.survey.update",
        ])){
            return rejectResponse();
        }

        return view('SurveyView::survey.create');
    }

    public function store(Request $request) {

        $data = $request->except('_token');

        $labels = [
            'name' => __('SurveyPanel::create.survey.label.name'),
            'status' => __('SurveyPanel::create.survey.label.status'),
            'start_date' => __('SurveyPanel::create.survey.label.start_date'),
            'result_time' => __('SurveyPanel::create.survey.label.result_time'),
            'voting_condition' => __('SurveyPanel::create.survey.label.voting_condition'),
            'same_ip' => __('SurveyPanel::create.survey.label.same_ip'),
            'same_cookie' => __('SurveyPanel::create.survey.label.same_cookie'),
        ];


        $this->validate($request, $this->rules, [], $labels);

        $data['admin_id'] = auth('admin')->id();
        $data['website_id'] = session()->get('panel.website')->id;

        $data['key'] = \Str::slug($data['name']);

        $survey = Survey::firstOrCreate($data);

        $cookieId = md5($survey->id);
        $survey->update(['cookie_id' => $cookieId]);

        return redirect()->route('Survey.index');
    }

    public function edit($id)
    {
        if(! activeUserCan([
            "survey.survey.update",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($id);

        return view('SurveyView::survey.edit', compact('survey'));
    }

    public function update($id, Request $request)
    {

        $data = $request->except('_token');

        $survey = Survey::find($id);

        if(is_null($survey)) {
            return redirect()->back()->withError(['message' => "Anket bulunamadı"]);
        }

        $labels = [
            'name' => __('SurveyPanel::create.survey.label.name'),
            'status' => __('SurveyPanel::create.survey.label.status'),
            'start_date' => __('SurveyPanel::create.survey.label.start_date'),
            'result_time' => __('SurveyPanel::create.survey.label.result_time'),
            'voting_condition' => __('SurveyPanel::create.survey.label.voting_condition'),
            'same_ip' => __('SurveyPanel::create.survey.label.same_ip'),
            'same_cookie' => __('SurveyPanel::create.survey.label.same_cookie'),
        ];


        $this->validate($request, $this->rules, [], $labels);

        $survey->update($data);

        return redirect()->route('Survey.edit', ['id' => $survey->id]);
    }

    public function delete($id)
    {
        if(! activeUserCan([
            "survey.survey.delete",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($id);

        if($survey) {
            $survey->delete();

            return redirect()->route('Survey.index');
        } else {
            return redirect()->back()->withError(['message' => "Anket bulunamadı"]);
        }
    }
}
