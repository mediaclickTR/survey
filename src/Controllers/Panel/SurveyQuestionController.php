<?php

namespace Mediapress\Survey\Controllers\Panel;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Survey\Models\Survey;
use Mediapress\Survey\Models\SurveyQuestion;
use Mediapress\Survey\Models\SurveyQuestionDetail;

class SurveyQuestionController extends Controller
{
    private $rules = [
        'order' => 'required',
        'status' => 'required',
        'answer_type' => 'required',
    ];

    public function index($surveyId)
    {
        if(! activeUserCan([
            "survey.survey.index",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($surveyId);

        if (is_null($survey)) {
            abort(404);
        }

        $questions = SurveyQuestion::where('survey_id', $surveyId)
            ->orderBy('order')
            ->get();

        return view('SurveyView::question.index', compact('survey', 'questions'));
    }

    public function create($surveyId)
    {
        if(! activeUserCan([
            "survey.survey.update",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($surveyId);

        if (is_null($survey)) {
            abort(404);
        }

        $otherQuestions = $survey->questions()
            ->where('status', 1)
            ->whereNull('survey_question_id')
            ->whereHas('detail')
            ->orderBy('order')
            ->get();

        $countryGroups = session('panel.website')->countryGroups;

        return view('SurveyView::question.create', compact('survey', 'otherQuestions','countryGroups'));
    }

    public function store($surveyId, Request $request)
    {

        $data = $request->except('_token');

        $labels = [
            'order' => __('SurveyPanel::create.question.label.order'),
            'status' => __('SurveyPanel::create.question.label.status'),
            'answer_type' => __('SurveyPanel::create.question.label.answer_type'),
        ];


        $this->validate($request, $this->rules, [], $labels);

        $data['survey_id'] = $surveyId;
        $data['admin_id'] = auth('admin')->id();

        $languages = $data['languages'];
        unset($data['languages']);

        $question = SurveyQuestion::firstOrCreate($data);

        $question->update(['key' => md5($question->id)]);

        foreach ($languages as $countryGroupId => $langs) {
            foreach ($langs as $languageId => $values) {
                if($values['name']) {
                    SurveyQuestionDetail::firstOrCreate(
                        [
                            'survey_question_id' => $question->id,
                            'country_group_id' => $countryGroupId,
                            'language_id' => $languageId,
                        ],
                        $values
                    );
                }
            }
        }

        return redirect()->route('Survey.question.index', ['surveyId' => $surveyId]);
    }

    public function edit($surveyId, $id)
    {
        if(! activeUserCan([
            "survey.survey.update",
        ])){
            return rejectResponse();
        }

        $survey = Survey::find($surveyId);
        $question = SurveyQuestion::find($id);
        $countryGroups = session('panel.website')->countryGroups;

        $otherQuestions = $survey->questions()
            ->where('status', 1)
            ->whereNull('survey_question_id')
            ->orderBy('order')
            ->get();


        return view('SurveyView::question.edit', compact('survey', 'question', 'countryGroups', 'otherQuestions'));
    }

    public function update($surveyId, $id, Request $request)
    {

        $data = $request->except('_token');

        $survey = Survey::find($surveyId);
        $question = SurveyQuestion::find($id);

        if (is_null($survey)) {
            return redirect()->back()->withError(['message' => "Anket bulunamadı"]);
        }
        if (is_null($question)) {
            return redirect()->back()->withError(['message' => "Soru bulunamadı"]);
        }

        $labels = [
            'order' => __('SurveyPanel::create.question.label.order'),
            'status' => __('SurveyPanel::create.question.label.status'),
            'answer_type' => __('SurveyPanel::create.question.label.answer_type'),
        ];


        $this->validate($request, $this->rules, [], $labels);

        $languages = $data['languages'];
        unset($data['languages']);

        $question->update($data);

        foreach ($languages as $countryGroupId => $langs) {
            foreach ($langs as $languageId => $values) {
                $question->details()
                    ->where('country_group_id', $countryGroupId)
                    ->where('language_id', $languageId)
                    ->update($values);
            }
        }

        return redirect()->route('Survey.question.index', ['surveyId' => $surveyId]);
    }

    public function delete($surveyId, $id)
    {
        if(! activeUserCan([
            "survey.survey.delete",
        ])){
            return rejectResponse();
        }
        $question = SurveyQuestion::find($id);

        if ($question) {
            $question->delete();

            return redirect()->route('Survey.question.index', ['surveyId' => $surveyId]);
        } else {
            return redirect()->back()->withError(['message' => "Soru bulunamadı"]);
        }
    }
}
