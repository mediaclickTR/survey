<?php

namespace Mediapress\Survey\Controllers\Web;

use Arcanedev\LogViewer\Controllers\LogViewerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Mediapress\Http\Controllers\PanelController as Controller;
use Mediapress\Survey\Models\Survey;
use Mediapress\Survey\Models\SurveyAnswer;
use Mediapress\Survey\Models\SurveyQuestion;
use Mediapress\Survey\Models\SurveyResult;
use Mediapress\Survey\Models\SurveyResultAnswer;

class SurveyController extends Controller
{
    private $ip;
    private $userAgent;

    /**
     * SurveyController constructor.
     */
    public function __construct()
    {
        $this->ip = getClientIp();
        $this->userAgent = getClientUserAgent();
    }

    public function save($surveyId, Request $request)
    {
        $survey = Survey::find($surveyId);

        if(is_null($survey)) {
            surveyLog($surveyId, [
                'function' => 'save',
                'file' => __CLASS__,
                'line' => __LINE__,
                'message' => "There is no survey for $surveyId."
            ]);
        }

        if ($survey->voting_condition == 2 && !auth()->check()) {
            return redirect()->back()->withErrors(['message' => langPartAttr('survey.error_message.voting_condition')])->withInput();
        }

        $resultIps = $survey->results->pluck('ip')->toArray();

        if ($survey->same_ip == 1 && in_array($this->ip, $resultIps)) {
            return redirect()->back()->withErrors(['message' => langPartAttr('survey.error_message.same_ip')])->withInput();
        }

        $cookieArray = getClientCookie();
        if ($survey->same_cookie == 1 && in_array($survey->cookie_id, $cookieArray)) {
            return redirect()->back()->withErrors(['message' => langPartAttr('survey.error_message.same_cookie')])->withInput();
        }


        $resultData = [
            'user_id' => auth()->id(),
            'survey_id' => $survey->id,
            'ip' => $this->ip,
            'user_agent' => $this->userAgent,
        ];
        $surveyResult = SurveyResult::firstOrCreate($resultData);

        $data = $this->getRequest($request, $surveyId);
        foreach ($data as $questionId => $answer) {

            $resultAnswerData = [
                'survey_result_id' => $surveyResult->id,
                'survey_question_id' => $questionId,
            ];

            $question = $survey->questions()->where('id', $questionId)->first();
            if ($question->answer_type == 3) {
                $resultAnswerData['custom_answer'] = $answer;
            } else {
                $resultAnswerData['survey_answer_id'] = $answer;
            }

            SurveyResultAnswer::firstOrCreate($resultAnswerData);
        }

        $this->saveCookie($survey->id);

        return redirect()->back()->with('survey_message', langPartAttr('survey.success_message'));
    }

    private function saveCookie(int $surveyId)
    {
        $minutes = 60 * 24 * 30;

        $cookie = getClientCookie();

        if(! in_array($surveyId, $cookie)) {
            $cookie[] = md5($surveyId);
        }

        Cookie::queue(Cookie::make('_srvy', encrypt($cookie), $minutes));
    }

    private function getRequest(Request $request, int $surveyId): array
    {
        $data = $request->except('_token');

        $return = [];
        foreach ($data as $questionKey => $answerKey) {
            $question = SurveyQuestion::where('survey_id', $surveyId)->where('key', $questionKey)->first();

            if (is_null($question)) {

                surveyLog($surveyId, [
                    'function' => 'getRequest',
                    'file' => __CLASS__,
                    'line' => __LINE__,
                    'message' => "There is no question for $questionKey."
                ]);

                throw new \Exception("There is no question for $questionKey.");
            }

            if ($question->answer_type == 3) {
                $answer = $answerKey;
            } else {
                $answer = SurveyAnswer::where('survey_id', $surveyId)->where('key', $answerKey)->first();
                if (is_null($answer)) {
                    surveyLog($surveyId, [
                        'function' => 'getRequest',
                        'file' => __CLASS__,
                        'line' => __LINE__,
                        'message' => "There is no answer for $answerKey."
                    ]);
                    throw new \Exception("There is no answer for $answerKey.");
                }
            }

            $return[$question->id] = $question->answer_type == 3 ? $answer : $answer->id;
        }

        return $return;
    }
}
