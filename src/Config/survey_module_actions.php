<?php
return [
    "survey" => [
        'name' => 'Survey::auth.sections.survey_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],
        'subs' => [
            "survey" => [
                'name' => 'Survey::auth.sections.survey',
                'name_affix' => "",
                'item_model' => \Mediapress\Survey\Survey::class,
                'item_id' => "*",
                "actions" => [
                    "index" => [
                        "name" => "Listele"
                    ],
                    "update" => [
                        "name" => "Güncelle"
                    ],
                    "delete" => [
                        "name" => "Sil"
                    ]
                ]
            ]
        ],
        'variations_title' => "",
        'variations' => []
    ]
];
