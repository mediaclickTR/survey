<?php

return [

    'question' => [
        'inputs' => [
            'cvar' => [
                'status' => false,
                'label' => ''
            ],
            'cint' => [
                'status' => false,
                'label' => ''
            ],
            'ctex' => [
                'status' => false,
                'label' => ''
            ]
        ],

        'detail_inputs' => [
            'detail' => [
                'status' => false,
                'label' => ''
            ],
            'cvar' => [
                'status' => false,
                'label' => ''
            ]
        ]
    ],

    'answer' => [
        'inputs' => [
            'cvar' => [
                'status' => false,
                'label' => ''
            ],
            'cint' => [
                'status' => false,
                'label' => ''
            ],
            'ctex' => [
                'status' => false,
                'label' => ''
            ]
        ],

        'detail_inputs' => [
            'detail' => [
                'status' => false,
                'label' => ''
            ],
            'cvar' => [
                'status' => false,
                'label' => ''
            ]
        ]
    ]
];
