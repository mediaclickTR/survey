<?php

return [

    'status' => [
        'active' => 'Aktif',
        'passive' => 'Pasif',
        'postdate' => 'İleri Tarihli'
    ],

    'survey' => [
        'label' => [
            'name' => 'Panel Adı',
            'status' => 'Yayınlanma Durumu',
            'start_date' => 'Başlangıç Tarihi',
            'finish_date' => 'Bitiş Tarihi',
            'result_time' => 'Oy Verdikten Sonra Sonuçları Gösterme Zamanı',
            'voting_condition' => 'Oy Verme Şartı',
            'same_ip' => 'Aynı IP ile Oy Verme Sayısı',
            'same_cookie' => 'Aynı Cihaz ile Oy Verme Sayısı',
        ],

        'result_time' => [
            'after' => 'Oy verdikten sonra göster',
            'close' => 'Anket kapanınca göster',
        ],

        'voting_condition' => [
            'all' => 'Herkes Kullanabilir',
            'login' => 'Login olduktan sonra kullanılabilir'
        ],

        'one' => 'Sadece 1 kez',
        'limitless' => 'Sınırsız',
    ],

    'question' => [
        'label' => [
            'other_question' => 'Diğer Sorular',
            'order' => 'Sıra',
            'status' => 'Yayınlanma Durumu',
            'type' => 'Cevap Tipi',
            'name' => 'Soru'
        ],

        'answer_type' => [
            'radio' => 'Çoktan Seçmeli',
            'image' => 'Görsel',
            'input' => 'Kullanıcı Girişi',
        ],

        'help_text' => "Eğer soru seçerseniz. Seçtiğiniz soru cevaplandıktan sonra bu soruyu cevaplamanız gerekmektedir."
    ],

    'answer' => [
        'label' => [
            'order' => 'Sıra',
            'status' => 'Yayınlanma Durumu',
            'name' => 'Cevap'
        ],
    ]
];
