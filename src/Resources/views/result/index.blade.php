@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title mb0">
            <div class="float-left">
                {!! $survey->name !!} | Sonuçlar
            </div>
            <div class="float-right mr-2">
                <a role="button" href="{!! route('Survey.index') !!}" class="btn btn-light">
                    <i class="fas fa-arrow-alt-left"></i>
                    Anket Listesi
                </a>
            </div>
        </div>
        <div class="row mt-5">
            @foreach($results as $result)
                <div class="col-md-6 mt-5">
                    <canvas id="myChart{{$loop->iteration}}"></canvas>
                </div>
            @endforeach
        </div>
        <div class="row">
            @foreach($results as $result)
                <div class="col-md-6 mt-5">
                    <table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Cevap</th>
                            <th>Oran</th>
                            <th>Sayı</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $total = 0;
                        @endphp
                        @foreach($result['answers'] as $id => $answer)
                            @php
                                $total += $answer['count'];
                            @endphp
                            <tr>
                                <td>{!! $loop->iteration !!}</td>
                                <td>{!! $answer['name'] !!}</td>
                                <td>{!! number_format($answer['rate'], 2) !!}</td>
                                <td>{!! $answer['count'] !!}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <th>Toplam</th>
                            <th></th>
                            <th></th>
                            <th class="float-right">{!! $total !!}</th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            @endforeach
        </div>

        @if(count($inputResults) > 0)
            <div class="row">
                @foreach($inputResults as $result)
                    <div class="col-md-6 mt-5">
                        <h4>{!! $result['name'] !!}</h4>
                        <table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Cevap</th>
                                <th>IP</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($result['answers'] as $answer)
                                <tr>
                                    <td>{!! $loop->iteration !!}</td>
                                    <td>{!! $answer['name'] !!}</td>
                                    <td>{!! $answer['ip'] !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
@endsection


@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.bundle.min.js"></script>

    <script>
        @foreach($results as $result)

        var ctx{{$loop->iteration}} = document.getElementById('myChart{{$loop->iteration}}');
        new Chart(ctx{{$loop->iteration}}, {
            type: 'pie',
            data: {
                labels: [{!! $result["answer_names"] !!}],
                datasets: [{
                    data: [{{ $result['answer_count'] }}],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 206, 86, 0.5)',
                        'rgba(75, 192, 192, 0.5)',
                        'rgba(153, 102, 255, 0.5)',
                        'rgba(255, 159, 64, 0.5)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 3
                }]
            },
            options: {
                title: {
                    display: true,
                    text: '{{ $result['name'] }}',
                    fontSize: 20
                },
            }
        });
        @endforeach
    </script>
@endpush
