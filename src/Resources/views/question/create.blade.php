@extends('ContentPanel::inc.module_main')

@php
    $questionInputs = config('survey.question.inputs');
    $questionDetailInputs = config('survey.question.detail_inputs');
@endphp

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title mb-0">Yeni Soru Ekle</div>
            </div>
            <div class="float-right">
                <a href="{{ route('Survey.question.index', ['surveyId' => $survey->id]) }}" class="btn btn-light">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="form" style="width: 600px !important; margin: 0 auto;">
                @include("MPCorePanel::inc.errors")
                <form action="{!! route('Survey.question.store', ['surveyId' => $survey->id]) !!}" method="POST">
                    @csrf

                    @if($otherQuestions->isNotEmpty())
                        <div class="form-group">
                            <label for="otherQuestions">{!! __('SurveyPanel::create.question.label.other_question') !!}</label>
                            <select class="form-control" id="otherQuestions" name="survey_question_id">
                                <option value="">{!! __('ContentPanel::general.select') !!}</option>
                                @foreach($otherQuestions as $otherQuestion)
                                    <option value="{!! $otherQuestion->id !!}">{!! strip_tags($otherQuestion->detail->name) !!}</option>
                                @endforeach
                            </select>
                            <small class="form-text text-muted">{!! __('SurveyPanel::create.question.help_text') !!}</small>
                        </div>
                    @endif

                    <div class="form-group row mt-3">
                        <label for="order" class="col-md-3">{!! __('SurveyPanel::create.question.label.order') !!}</label>
                        <input type="number"
                               name="order"
                               id="order"
                               class="form-control col-md-4"
                               value="{!! old('order') !!}">
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('SurveyPanel::create.question.label.status') !!}</label>
                        <div class="checkbox">
                            <label for="active">
                                <input type="radio"
                                       name="status"
                                       id="active"
                                       value="1"
                                    {{ old('status', 1) == 1 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.status.active') !!}
                            </label>
                            <label for="passive">
                                <input type="radio"
                                       name="status"
                                       id="passive"
                                       value="2"
                                    {{ old('status') == 2 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.status.passive') !!}
                            </label>
                        </div>
                        <hr>
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('SurveyPanel::create.question.label.type') !!}</label>
                        <div class="checkbox col-md-12">
                            <label for="type_radio">
                                <input type="radio"
                                       name="answer_type"
                                       id="type_radio"
                                       value="1"
                                    {{ old('answer_type', 1) == 1 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.question.answer_type.radio') !!}
                            </label>
                        </div>
                        <div class="checkbox col-md-12">
                            <label for="type_image">
                                <input type="radio"
                                       name="answer_type"
                                       id="type_image"
                                       value="2"
                                    {{ old('answer_type') == 2 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.question.answer_type.image') !!}
                            </label>
                        </div>
                        <div class="checkbox col-md-12">
                            <label for="type_input">
                                <input type="radio"
                                       name="answer_type"
                                       id="type_input"
                                       value="3"
                                    {{ old('answer_type') == 3 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.question.answer_type.input') !!}
                            </label>
                        </div>
                    </div>

                    @if($questionInputs['cint']['status'] ?? false)
                        <div class="form-group">
                            <label for="cint">{!! $questionInputs['cint']['label'] !!}</label>
                            <input type="number"
                                   name="cint"
                                   id="cint"
                                   class="form-control"
                                   value="{!! old('cint') !!}">
                        </div>
                    @endif

                    @if($questionInputs['cvar']['status'] ?? false)
                        <div class="form-group">
                            <label for="cvar">{!! $questionInputs['cvar']['label'] !!}</label>
                            <input type="text"
                                   name="cvar"
                                   id="cvar"
                                   class="form-control"
                                   value="{!! old('cvar') !!}">
                        </div>
                    @endif


                    @if($questionInputs['ctex']['status'] ?? false)
                        <div class="form-group">
                            <label for="ctex">{!! $questionInputs['ctex']['label'] !!}</label>
                        </div>
                        <div class="form-group">
                            <textarea id="ctex"
                                      name="ctex">{!! old('ctex') !!}</textarea>
                        </div>
                    @endif



                    <div class="languages float-left w-100">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">

                            @foreach($countryGroups as $countryGroup)
                                @foreach($countryGroup->languages as $language)
                                    <li>
                                        <a {!! $loop->first && $loop->parent->first ? 'class="show active"' :'' !!} data-toggle="tab"
                                           href="#{!! $countryGroup->code . $language->code !!}">
                                            <img src="{!! asset('vendor/mediapress/images/flags/'.$language->flag) !!}"
                                                 height="13" alt=""> {!! strtoupper($language->code) . " (" . $countryGroup->title . ")" !!}
                                        </a>
                                    </li>
                                @endforeach
                            @endforeach
                        </ul>
                        <div class="tab-content" id="myTabContent">

                            @foreach($countryGroups as $countryGroup)
                                @foreach($countryGroup->languages as $language)
                                    <div id="{!! $countryGroup->code . $language->code !!}"
                                         class="tab-pane fade in  {!! $loop->first && $loop->parent->first ? 'show active' :'' !!}">

                                        <div class="form-group">
                                            <label for="question">{!! __('SurveyPanel::create.question.label.name') !!}</label>
                                        </div>
                                        <div class="form-group">
                                            <textarea id="question{{$countryGroup->id . $language->id}}"
                                                      name="languages[{{$countryGroup->id}}][{!! $language->id !!}][name]">{!! old('languages.' . $countryGroup->id . '.' . $language->id . '.name') !!}</textarea>
                                        </div>

                                        @if($questionDetailInputs['detail']['status'] ?? false)
                                            <div class="form-group">
                                                <label for="detail">{!! $questionDetailInputs['detail']['label'] !!}</label>
                                            </div>
                                            <div class="form-group">
                                                <textarea id="detail"
                                                          name="languages[{{$countryGroup->id}}][{!! $language->id !!}][detail]">{!! old('languages.' . $countryGroup->id . '.' . $language->id . '.detail') !!}</textarea>
                                            </div>
                                        @endif

                                        @if($questionDetailInputs['cvar']['status'] ?? false)
                                            <div class="form-group">
                                                <label for="detail_cvar">{!! $questionDetailInputs['cvar']['label'] !!}</label>
                                                <input type="text"
                                                       name="languages[{{$countryGroup->id}}][{!! $language->id !!}][cvar]"
                                                       id="detail_cvar"
                                                       class="form-control"
                                                       value="{!! old('languages.' . $countryGroup->id . '.' . $language->id . '.cvar') !!}">
                                            </div>
                                        @endif

                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </div>

                    <div class="submit">
                        <button class="btn btn-primary">{!! __('ContentPanel::general.save') !!}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push("scripts")
    <script src="{!! asset('vendor/mediapress/js/plugins-cke/ckeditor/ckeditor.js') !!}"></script>

    <script>
        $('textarea').each(function (key, item) {
            CKEDITOR.replace($(this).attr('id'),
                {
                    language: 'tr',
                    height: 200,
                    resize_enabled: false,
                    toolbar: [
                        {
                            name: 'clipboard',
                            groups: ['undo'],
                            items: ['Undo', 'Redo']
                        },
                        {name: 'links', items: ['Link', 'Unlink']},
                        {
                            name: 'basicstyles',
                            groups: ['basicstyles'],
                            items: ['Bold', 'Italic', 'Underline', 'Strike']
                        },
                        {name: 'styles', items: ['Styles', 'Format', 'FontSize']},

                    ]
                });
            CKEDITOR.config.entities_latin=false
        });
    </script>
@endpush
