@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title mb0">
            <div class="float-left">
                {!! $survey->name !!} | Sorular
            </div>
            <div class="float-right">
                <a role="button" href="{!! route('Survey.question.create', ['surveyId' => $survey->id]) !!}" class="btn btn-primary">
                    <i class="fas fa-plus"></i>
                    {!! trans("ContentPanel::general.add_new") !!}
                </a>
            </div>
            <div class="float-right mr-2">
                <a role="button" href="{!! route('Survey.edit', ['id' => $survey->id]) !!}" class="btn btn-warning">
                    <i class="fas fa-cog"></i>
                    Anket Ayarları
                </a>
            </div>
            <div class="float-right mr-2">
                <a role="button" href="{!! route('Survey.index') !!}" class="btn btn-light">
                    <i class="fas fa-arrow-alt-left"></i>
                    Anket Listesi
                </a>
            </div>
        </div>
        <table>
            <thead>
            <tr>
                <th>Sıra</th>
                <th style="width: 10%;">Durum</th>
                <th style="width: 10%;">Cevap Türü</th>
                <th style="width: 40%">Soru</th>
                <th>Ekleme Tarihi</th>
                <th>Ekleyen</th>
                <th>İşlemler</th>
            </tr>
            </thead>
            <tbody>
                @foreach($questions as $question)
                    <tr>
                        <td>{!! $question->order !!}</td>
                        <td class="status">
                            @if($question->status == 1)
                                <i class="fa fa-docker" style="background: #7fcc46"></i> {{ "Yayında" }}
                            @else
                                <i class="fa fa-docker" style="background: #dc3545"></i> {{ "Pasif" }}
                            @endif
                        </td>
                        <td>
                            <strong>
                                @if($question->answer_type == 1)
                                    Çoktan Seçmeli
                                @elseif($question->answer_type == 2)
                                    Görsel
                                @else
                                    Kullanıcı Girişi
                                @endif
                            </strong>
                        </td>
                        <td>{!! strip_tags($question->detail->name) !!}</td>
                        <td>{!! $question->created_at !!}</td>
                        <td>{!! optional($question->admin)->username !!}</td>
                        <td>
                            <a href="{!! route('Survey.question.edit', ['surveyId' => $survey->id, 'id' => $question->id]) !!}"
                               class="mr-2" title="{!! trans("MPCorePanel::general.edit") !!}">
                                <i class="fa fa-edit"></i>
                            </a>
                            @if($question->answer_type != 3)
                            <a href="{!! route('Survey.answer.index', ['surveyId' => $survey->id, 'questionId' => $question->id]) !!}" class="mr-2" title="Cevaplar">
                                <i class="fa fa-reply"></i>
                            </a>
                            @endif
                            <a href="javascript:void(0);" class="deleteBtn" data-url="{!! route('Survey.question.delete', ['surveyId' => $survey->id, 'id' => $question->id]) !!}" title="{!! trans("MPCorePanel::general.delete") !!}">
                                <i class="fa fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection


@push('scripts')
    <script>
        $('.deleteBtn').on('click', function () {
            var popupDeleteUrl = $(this).attr('data-url');
            Swal.fire({
                title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                text: '{{ trans("MPCorePanel::general.action_cannot_undone") }}',
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{!! trans("MPCorePanel::general.yes") !!}',
                cancelButtonText: '{!! trans("MPCorePanel::general.no") !!}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (dismiss) {
                if (dismiss.value) {
                    window.location = popupDeleteUrl;
                }
            });
        })
    </script>
@endpush
