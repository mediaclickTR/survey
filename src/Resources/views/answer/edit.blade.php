@extends('ContentPanel::inc.module_main')

@php
    $answerInputs = config('survey.answer.inputs');
    $answerDetailInputs = config('survey.answer.detail_inputs');
@endphp

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title mb-0">Yeni Cevap Ekle</div>
            </div>
            <div class="float-right">
                <a href="{{ route('Survey.answer.index', ['surveyId' => $survey->id, 'questionId' => $question->id]) }}" class="btn btn-light">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="form" style="width: 600px !important; margin: 0 auto;">
                @include("MPCorePanel::inc.errors")
                <form
                    action="{!! route('Survey.answer.update', ['surveyId' => $survey->id, 'questionId' => $question->id, 'id' => $answer->id]) !!}"
                    method="POST">
                    @csrf


                    <div class="form-group row mt-3">
                        <label for="order"
                               class="col-md-3">{!! __('SurveyPanel::create.answer.label.order') !!}</label>
                        <input type="number"
                               name="order"
                               id="order"
                               class="form-control col-md-4"
                               value="{!! old('order', $answer->id) !!}">
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('SurveyPanel::create.answer.label.status') !!}</label>
                        <div class="checkbox">
                            <label for="active">
                                <input type="radio"
                                       name="status"
                                       id="active"
                                       value="1"
                                    {{ old('status', $answer->status) == 1 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.status.active') !!}
                            </label>
                            <label for="passive">
                                <input type="radio"
                                       name="status"
                                       id="passive"
                                       value="2"
                                    {{ old('status', $answer->status) == 2 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.status.passive') !!}
                            </label>
                        </div>
                        <hr>
                    </div>

                    @if($answerInputs['cint']['status'] ?? false)
                        <div class="form-group">
                            <label for="cint">{!! $answerInputs['cint']['label'] !!}</label>
                            <input type="number"
                                   name="cint"
                                   id="cint"
                                   class="form-control"
                                   value="{!! old('cint', $answer->cint) !!}">
                        </div>
                    @endif

                    @if($answerInputs['cvar']['status'] ?? false)
                        <div class="form-group">
                            <label for="cvar">{!! $answerInputs['cvar']['label'] !!}</label>
                            <input type="text"
                                   name="cvar"
                                   id="cvar"
                                   class="form-control"
                                   value="{!! old('cvar', $answer->cvar) !!}">
                        </div>
                    @endif


                    @if($answerInputs['ctex']['status'] ?? false)
                        <div class="form-group">
                            <label for="ctex">{!! $answerInputs['ctex']['label'] !!}</label>
                        </div>
                        <div class="form-group">
                            <textarea id="ctex"
                                      name="ctex">{!! old('ctex', $answer->ctex) !!}</textarea>
                        </div>
                    @endif

                    @if($question->answer_type == 2)
                        <div class="form-group">
                            <?php
                            $options = json_decode('{"key":"survey","file_type":"image","allow_actions":["select","upload"],"allow_diskkeys":["azure","local"],"extensions":"JPG,JPEG,PNG,GIF,SVG,SVG","min_width":"","max_width":"","min_height":"","max_height":"","min_filesize":"","max_filesize":"5120","max_file_count":"1","additional_rules":""}', 1);
                            ?>
                            <label for="survey">Görsel</label>
                            <div class="file-box file-manager" id="survey"
                                 data-options='{!! json_encode($options) !!}'>
                                <input name="image" type="hidden"
                                       value="[{{ $answer->image }}]">
                                <a href="javascript:void(0);"
                                   onclick="javascript:addImage($(this))">
                                    <span class="file-icon"></span>
                                    <i class="mx">Max dosya boyutu 5 MB</i>
                                </a>
                            </div>
                        </div>
                    @endif


                    @if($question->answer_type != 3 || $answerDetailInputs['detail']['status'] || $answerDetailInputs['cvar']['status'])
                        <div class="languages float-left w-100">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">

                                @foreach($countryGroups as $countryGroup)
                                    @foreach($countryGroup->languages as $language)
                                        <li>
                                            <a {!! $loop->first && $loop->parent->first ? 'class="show active"' :'' !!} data-toggle="tab"
                                               href="#{!! $countryGroup->code . $language->code !!}">
                                                <img
                                                    src="{!! asset('vendor/mediapress/images/flags/'.$language->flag) !!}"
                                                    height="13"
                                                    alt=""> {!! strtoupper($language->code) . " (" . $countryGroup->title . ")" !!}
                                            </a>
                                        </li>
                                    @endforeach
                                @endforeach
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                @foreach($countryGroups as $countryGroup)
                                    @foreach($countryGroup->languages as $language)
                                        <div id="{!! $countryGroup->code . $language->code !!}"
                                             class="tab-pane fade in  {!! $loop->first && $loop->parent->first ? 'show active' :'' !!}">
                                            @php
                                                $answerDetail = $answer->details()
                                                                    ->where('country_group_id', $countryGroup->id)
                                                                    ->where('language_id', $language->id)
                                                                    ->first();
                                            @endphp
                                            @if($question->answer_type != 3)
                                                <div class="form-group">
                                                    <label for="answer">{!! __('SurveyPanel::create.answer.label.name') !!}</label>
                                                </div>
                                                <div class="form-group">
                                                <textarea id="answer{{$countryGroup->id . $language->id}}"
                                                          name="languages[{{$countryGroup->id}}][{!! $language->id !!}][name]">{!! old('languages.' . $countryGroup->id . '.' . $language->id . '.name', $answerDetail ? $answerDetail->name : '') !!}</textarea>
                                                </div>
                                            @endif

                                            @if($answerDetailInputs['detail']['status'] ?? false)
                                                <div class="form-group">
                                                    <label
                                                        for="detail">{!! $answerDetailInputs['detail']['label'] !!}</label>
                                                </div>
                                                <div class="form-group">
                                                <textarea id="detail"
                                                          name="languages[{{$countryGroup->id}}][{!! $language->id !!}][detail]">{!! old('languages.' . $countryGroup->id . '.' . $language->id . '.detail', $answerDetail ? $answerDetail->detail : '') !!}</textarea>
                                                </div>
                                            @endif

                                            @if($answerDetailInputs['cvar']['status'] ?? false)
                                                <div class="form-group">
                                                    <label
                                                        for="detail_cvar">{!! $answerDetailInputs['cvar']['label'] !!}</label>
                                                    <input type="text"
                                                           name="languages[{{$countryGroup->id}}][{!! $language->id !!}][cvar]"
                                                           id="detail_cvar"
                                                           class="form-control"
                                                           value="{!! old('languages.' . $countryGroup->id . '.' . $language->id . '.cvar', $answerDetail ? $answerDetail->cvar : '') !!}">
                                                </div>
                                            @endif

                                        </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    @endif

                    <div class="submit">
                        <button class="btn btn-primary">{!! __('ContentPanel::general.save') !!}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@push("scripts")
    <script src="{!! asset('vendor/mediapress/js/plugins-cke/ckeditor/ckeditor.js') !!}"></script>

    <script>
        $('textarea').each(function (key, item) {
            CKEDITOR.replace($(this).attr('id'),
                {
                    language: 'tr',
                    height: 200,
                    resize_enabled: false,
                    toolbar: [
                        {
                            name: 'clipboard',
                            groups: ['undo'],
                            items: ['Undo', 'Redo']
                        },
                        {name: 'links', items: ['Link', 'Unlink']},
                        {
                            name: 'basicstyles',
                            groups: ['basicstyles'],
                            items: ['Bold', 'Italic', 'Underline', 'Strike']
                        },
                        {name: 'styles', items: ['Styles', 'Format', 'FontSize']},

                    ]
                });
            CKEDITOR.config.entities_latin=false
        });
    </script>
@endpush

@if($question->answer_type == 2)
    @if(function_exists('customFilemanager'))
        @push('scripts')
            {!! customFilemanager() !!}
        @endpush
    @endif

    @push('styles')
        <link rel="stylesheet" href="{{asset("/vendor/mediapress/css/sortable.css")}}"/>
        <style>
            .file-manager {
                border: 2px solid #E5E5E5;
                height: 181px;
                width: 230px;
            }

            .file-manager-image {
                height: 100%;
            }

            .file-manager-image img {
                height: 100%;
            }

            i.file-manager-rule {
                position: absolute;
                left: 0;
                bottom: 48px;
                text-align: center;
                display: block;
                width: 100%;
                font: 12px "Poppins", sans-serif;
                color: #393939;
            }

            ul.file-manager-options {
                position: initial !important;
            }

            ul.file-manager-options li {
                width: 100% !important;
                padding: 0 10px;
                float: none;
            }

            ul.file-manager-options li u {
                text-align: left !important;
            }
        </style>
    @endpush
@endif
