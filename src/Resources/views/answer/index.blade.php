@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title mb0">
            <div class="float-left">
                {!! $survey->name !!} | Cevaplar
            </div>
            <div class="float-right">
                <a role="button" href="{!! route('Survey.answer.create', ['surveyId' => $survey->id, 'questionId' => $question->id,]) !!}" class="btn btn-primary">
                    <i class="fas fa-plus"></i>
                    {!! trans("ContentPanel::general.add_new") !!}
                </a>
            </div>
            <div class="float-right mr-2">
                <a role="button" href="{!! route('Survey.question.edit', ['surveyId' => $survey->id, 'id' => $question->id]) !!}" class="btn btn-warning">
                    <i class="fas fa-cog"></i>
                    Soru Ayarları
                </a>
            </div>
            <div class="float-right mr-2">
                <a role="button" href="{!! route('Survey.question.index', ['surveyId' => $survey->id]) !!}" class="btn btn-light">
                    <i class="fas fa-arrow-alt-left"></i>
                    Soru Listesi
                </a>
            </div>
        </div>
        <table>
            <thead>
            <tr>
                <th>Sıra</th>
                <th>Durum</th>
                <th class="w-50">Cevap</th>
                <th>Ekleme Tarihi</th>
                <th>Ekleyen</th>
                <th>İşlemler</th>
            </tr>
            </thead>
            <tbody>
                @foreach($answers as $answer)
                    <tr>
                        <td>{!! $answer->order !!}</td>
                        <td class="status">
                            @if($answer->status == 1)
                                <i class="fa fa-docker" style="background: #7fcc46"></i> {{ "Yayında" }}
                            @else
                                <i class="fa fa-docker" style="background: #dc3545"></i> {{ "Pasif" }}
                            @endif
                        </td>
                        <td>
                        @if($question->answer_type == 1)
                            {!! strip_tags($answer->detail->name) !!}
                        @elseif($question->answer_type == 2)
                            <img src="{!! image($answer->image) !!}" height="50" alt="">
                        @else
                            Input
                        @endif
                        </td>
                        <td>{!! $answer->created_at !!}</td>
                        <td>{!! optional($answer->admin)->username !!}</td>
                        <td>
                            <a href="{!! route('Survey.answer.edit', ['surveyId' => $survey->id, 'questionId' => $question->id, 'id' => $answer->id]) !!}"
                               class="mr-2" title="{!! trans("MPCorePanel::general.edit") !!}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="javascript:void(0);" class="deleteBtn" data-url="{!! route('Survey.answer.delete', ['surveyId' => $survey->id, 'questionId' => $question->id, 'id' => $answer->id]) !!}" title="{!! trans("MPCorePanel::general.delete") !!}">
                                <i class="fa fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection


@push('scripts')
    <script>
        $('.deleteBtn').on('click', function () {
            var popupDeleteUrl = $(this).attr('data-url');
            Swal.fire({
                title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                text: '{{ trans("MPCorePanel::general.action_cannot_undone") }}',
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{!! trans("MPCorePanel::general.yes") !!}',
                cancelButtonText: '{!! trans("MPCorePanel::general.no") !!}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (dismiss) {
                if (dismiss.value) {
                    window.location = popupDeleteUrl;
                }
            });
        })
    </script>
@endpush
