@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content">
        <div class="title mb0">
            <div class="float-left">
                Anketler
            </div>
            <div class="float-right">
                <a role="button" href="{!! route('Survey.create') !!}" class="btn btn-primary">
                    <i class="fas fa-plus"></i>
                    {!! trans("ContentPanel::general.add_new") !!}
                </a>
            </div>
        </div>
        <table>
            <thead>
            <tr>
                <th>#</th>
                <th>Durum</th>
                <th>Anket Adı</th>
                <th>Kapanış Tarihi</th>
                <th>Ekleme Tarihi</th>
                <th>Ekleyen</th>
                <th>İşlemler</th>
            </tr>
            </thead>
            <tbody>
                @foreach($surveys as $survey)
                    <tr>
                        <td>{!! $survey->id !!}</td>
                        <td class="status">
                            @if($survey->status == 1)
                                <i class="active" style="background: #7fcc46"></i> {{ "Yayında" }}
                            @elseif($survey->status == 4)
                                <i class="passive" style="background: #dc3545"></i> {{ "İleri Tarihli" }}
                            @else
                                <i class="passive" style="background: #dc3545"></i> {{ "Pasif" }}
                            @endif
                        </td>
                        <td>{!! $survey->name !!}</td>
                        <td>{!! $survey->finish_date !!}</td>
                        <td>{!! $survey->created_at !!}</td>
                        <td>{!! optional($survey->admin)->username !!}</td>
                        <td>
                            <a href="{!! route('Survey.question.index', ['surveyId' => $survey->id]) !!}" class="mr-2" title="Sorular">
                                <i class="fa fa-question"></i>
                            </a>
                            <a href="{!! route('Survey.result.index', ['surveyId' => $survey->id]) !!}" class="mr-2" title="Sonuçlar">
                                <i class="fa fa-trophy"></i>
                            </a>
                            <a href="{!! route('Survey.edit', ['id' => $survey->id]) !!}" class="mr-2" title="{!! trans("MPCorePanel::general.edit") !!}">
                                <i class="fa fa-edit"></i>
                            </a>
                            <a href="javascript:void(0);" class="deleteBtn" data-url="{!! route('Survey.delete', ['id' => $survey->id]) !!}" title="{!! trans("MPCorePanel::general.delete") !!}">
                                <i class="fa fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection


@push('scripts')
    <script>
        $('.deleteBtn').on('click', function () {
            var popupDeleteUrl = $(this).attr('data-url');
            Swal.fire({
                title: '{{ trans("MPCorePanel::general.are_you_sure") }}',
                text: '{{ trans("MPCorePanel::general.action_cannot_undone") }}',
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: '{!! trans("MPCorePanel::general.yes") !!}',
                cancelButtonText: '{!! trans("MPCorePanel::general.no") !!}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (dismiss) {
                if (dismiss.value) {
                    window.location = popupDeleteUrl;
                }
            });
        })
    </script>
@endpush
