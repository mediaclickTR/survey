@extends('ContentPanel::inc.module_main')

@section('content')
    @include('MPCorePanel::inc.breadcrumb')
    <div class="page-content p-0">
        <div class="topPage">
            <div class="float-left">
                <div class="title mb-0">Yeni Anket Ekle</div>
            </div>
            <div class="float-right">
                <a href="{{ route('Survey.index') }}" class="btn btn-light">
                    <i class="fa fa-chevron-left"></i>
                    {!! __('ContentPanel::general.go_back_list') !!}
                </a>
            </div>
        </div>
        <div class="p-30 mt-4">
            <div class="form center">
                @include("MPCorePanel::inc.errors")
                <form action="{!! route('Survey.store') !!}" method="post">
                    @csrf

                    <div class="form-group">
                        <label for="name">{!! __('SurveyPanel::create.survey.label.name') !!}</label>
                        <input name="name" type="text" value="{!! old('name') !!}">
                    </div>
                    <div class="form-group">
                        <label for="key">Key</label>
                        <input name="key" disabled="disabled" value="{!! old('key') !!}">
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('SurveyPanel::create.survey.label.status') !!}</label>
                        <div class="checkbox">
                            <label for="active">
                                <input type="radio"
                                       name="status"
                                       id="active"
                                       value="1"
                                    {{ old('status', 1) == 1 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.status.active') !!}
                            </label>
                            <label for="passive">
                                <input type="radio"
                                       name="status"
                                       id="passive"
                                       value="2"
                                    {{ old('status') == 2 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.status.passive') !!}
                            </label>
                            <label for="postdate">
                                <input type="radio"
                                       name="status"
                                       id="postdate"
                                       value="3"
                                    {{ old('status') == 3 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.status.postdate') !!}
                            </label>
                        </div>
                    </div>

                    <div class="form-group {{ old('status') == 3 ? '' : 'd-none' }}" id="startDate">
                        <label for="name">{!! __('SurveyPanel::create.survey.label.start_date') !!}</label>
                        <input type="date"
                               name="start_date"
                               min="{{ date('Y-m-d') }}"
                               value="{!! old('start_date') !!}" style="width: 300px">
                    </div>

                    <div class="form-group">
                        <label for="name">{!! __('SurveyPanel::create.survey.label.finish_date') !!}</label>
                        <input type="date"
                               name="finish_date"
                               min="{{ date('Y-m-d') }}"
                               value="{!! old('finish_date') !!}" style="width: 300px">
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('SurveyPanel::create.survey.label.result_time') !!}</label>
                        <div class="checkbox col-md-12">
                            <label for="result_time_after">
                                <input type="radio"
                                       name="result_time"
                                       id="result_time_after"
                                       value="1"
                                    {{ old('result_time', 1) == 1 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.survey.result_time.after') !!}
                            </label>
                        </div>
                        <div class="checkbox col-md-12">
                            <label for="result_time_close">
                                <input type="radio"
                                       name="result_time"
                                       id="result_time_close"
                                       value="2"
                                    {{ old('result_time') == 2 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.survey.result_time.close') !!}
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('SurveyPanel::create.survey.label.voting_condition') !!}</label>
                        <div class="checkbox col-md-12">
                            <label for="voting_condition_all">
                                <input type="radio"
                                       name="voting_condition"
                                       id="voting_condition_all"
                                       value="1"
                                    {{ old('voting_condition', 1) == 1 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.survey.voting_condition.all') !!}
                            </label>
                        </div>
                        <div class="checkbox col-md-12">
                            <label for="voting_condition_login">
                                <input type="radio"
                                       name="voting_condition"
                                       id="voting_condition_login"
                                       value="2"
                                    {{ old('voting_condition') == 2 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.survey.voting_condition.login') !!}
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('SurveyPanel::create.survey.label.same_ip') !!}</label>
                        <div class="checkbox col-md-12">
                            <label for="same_ip_one">
                                <input type="radio"
                                       name="same_ip"
                                       id="same_ip_one"
                                       value="1"
                                    {{ old('same_ip', 1) == 1 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.survey.one') !!}
                            </label>
                        </div>
                        <div class="checkbox col-md-12">
                            <label for="same_ip_limitless">
                                <input type="radio"
                                       name="same_ip"
                                       id="same_ip_limitless"
                                       value="2"
                                    {{ old('same_ip') == 2 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.survey.limitless') !!}
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="image-radio">{!! __('SurveyPanel::create.survey.label.same_cookie') !!}</label>
                        <div class="checkbox col-md-12">
                            <label for="same_cookie_one">
                                <input type="radio"
                                       name="same_cookie"
                                       id="same_cookie_one"
                                       value="1"
                                    {{ old('same_cookie', 1) == 1 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.survey.one') !!}
                            </label>
                        </div>
                        <div class="checkbox col-md-12">
                            <label for="same_cookie_limitless">
                                <input type="radio"
                                       name="same_cookie"
                                       id="same_cookie_limitless"
                                       value="2"
                                    {{ old('same_cookie') == 2 ? 'checked' : '' }}>
                                {!! __('SurveyPanel::create.survey.limitless') !!}
                            </label>
                        </div>
                    </div>

                    <div class="submit">
                        <button class="btn btn-primary">{!! __('ContentPanel::general.save') !!}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push("scripts")
    <script>
        $('[name="name"]').on('keyup', function () {
            var value = $(this).val();
            $('[name="key"]').val(slugify(value));
        });
        function slugify(text)
        {
            return text.toString().toLowerCase()
                .replace(/\s+/g, '-')           // Replace spaces with -
                .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                .replace(/^-+/, '')             // Trim - from start of text
                .replace(/-+$/, '');            // Trim - from end of text
        }

        $('[name="status"]').on('ifChanged', function () {
            if ($(this).val() == 3) {
                $('#startDate').removeClass('d-none');
            } else {
                $('#startDate').addClass('d-none');
                $('#startDate input').val('');
            }
        });
    </script>
@endpush
