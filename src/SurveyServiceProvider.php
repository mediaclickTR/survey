<?php

namespace Mediapress\Survey;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Support\Facades\Route;


class SurveyServiceProvider extends ServiceProvider
{
    public $namespace = 'Mediapress\Survey';

    public function boot()
    {
        $this->map();
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views', 'SurveyView');
        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Resources/lang', 'SurveyPanel');
        $this->loadMigrationsFrom(__DIR__ . DIRECTORY_SEPARATOR . 'Database' . DIRECTORY_SEPARATOR . 'migrations');
        $this->publishes([__DIR__ . '/..' . DIRECTORY_SEPARATOR . 'Config/survey.php' => config_path()], 'SurveyPublishes');

        $this->publishes([__DIR__ . '/Assets' => public_path('vendor/survey')]);


        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'Http' . DIRECTORY_SEPARATOR . "helpers.php")) {
            include_once __DIR__ . DIRECTORY_SEPARATOR . 'Http' . DIRECTORY_SEPARATOR . "helpers.php";
        }

        $this->mergeConfig(__DIR__ . '/Config/survey_module_actions.php', 'survey_module_actions');
    }

    public function register()
    {
        //
    }


    public function map()
    {
        $this->mapPanelRoutes();
        $this->mapWebRoutes();
    }

    protected function mapPanelRoutes()
    {
        $routes_file = __DIR__ . DIRECTORY_SEPARATOR . 'Routes' . DIRECTORY_SEPARATOR . 'panel.php';
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace . '\Controllers\Panel',
            'prefix' => 'mp-admin',
        ], function ($router) use ($routes_file) {
            if (is_file($routes_file)) {
                include_once $routes_file;
            }
        });
    }

    protected function mapWebRoutes()
    {
        $routes_file = __DIR__ . DIRECTORY_SEPARATOR . 'Routes' . DIRECTORY_SEPARATOR . 'web.php';
        Route::group([
            'middleware' => ['web', 'mediapress.after'],
            'namespace' => $this->namespace . '\Controllers\Web',
        ], function ($router) use ($routes_file) {
            if (is_file($routes_file)) {
                include_once $routes_file;
            }
        });
    }
}
