<?php

Route::group(['prefix' => 'Survey', 'middleware' => 'panel.auth', 'as' => 'Survey.'], function () {
    Route::get('/', 'SurveyController@index')->name('index');
    Route::get('/create', 'SurveyController@create')->name('create');
    Route::post('/store', 'SurveyController@store')->name('store');
    Route::get('/edit/{id}', 'SurveyController@edit')->name('edit');
    Route::post('/update/{id}', 'SurveyController@update')->name('update');
    Route::get('/delete/{id}', 'SurveyController@delete')->name('delete');


    Route::group(['prefix' => '{surveyId}/Question', 'middleware' => 'panel.auth', 'as' => 'question.'], function () {
        Route::get('/', 'SurveyQuestionController@index')->name('index');
        Route::get('/create', 'SurveyQuestionController@create')->name('create');
        Route::post('/store', 'SurveyQuestionController@store')->name('store');
        Route::get('/edit/{id}', 'SurveyQuestionController@edit')->name('edit');
        Route::post('/update/{id}', 'SurveyQuestionController@update')->name('update');
        Route::get('/delete/{id}', 'SurveyQuestionController@delete')->name('delete');
    });

    Route::group(['prefix' => '{surveyId}/Question/{questionId}/Answer', 'middleware' => 'panel.auth', 'as' => 'answer.'], function () {
        Route::get('/', 'SurveyAnswerController@index')->name('index');
        Route::get('/create', 'SurveyAnswerController@create')->name('create');
        Route::post('/store', 'SurveyAnswerController@store')->name('store');
        Route::get('/edit/{id}', 'SurveyAnswerController@edit')->name('edit');
        Route::post('/update/{id}', 'SurveyAnswerController@update')->name('update');
        Route::get('/delete/{id}', 'SurveyAnswerController@delete')->name('delete');
    });


    Route::group(['prefix' => '{surveyId}/Results', 'middleware' => 'panel.auth', 'as' => 'result.'], function () {
        Route::get('/', 'SurveyResultController@index')->name('index');
    });
});
