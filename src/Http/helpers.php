<?php

/**
 * @param string $key
 * @return \Mediapress\Survey\Foundation\SurveyKit
 */
function survey(string $key) {
    $survey = new \Mediapress\Survey\Foundation\SurveyKit();

    return $survey->setKey($key)->init();
}

/**
 * @param int|null $surveyId
 * @param array $exceptionArr
 */
function surveyLog(int $surveyId = null, array $exceptionArr) {
    $exceptionArr['survey_id'] = $surveyId;
    \Mediapress\Survey\Models\SurveyLog::create($exceptionArr);
}

/**
 * @param int $surveyId
 * @return mixed
 */
function surveySendUrl(int $surveyId) {
    return route('surveySave', ['surveyId' => $surveyId]);
}

/**
 * @return mixed
 */
function getClientIp() {

    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
  $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}
    return $_SERVER['REMOTE_ADDR'];
}

/**
 * @return array
 */
function getClientCookie() {
    $cookie = Illuminate\Support\Facades\Cookie::get('_srvy');

    if($cookie) {
        return decrypt($cookie);
    }
    return [];
}

/**
 * @return mixed
 */
function getClientUserAgent() {
    return request()->userAgent();
}
