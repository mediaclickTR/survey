<?php
/**
 * Created by PhpStorm.
 * User: eraye
 * Date: 27.01.2019
 * Time: 04:48
 */

namespace Mediapress\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Content\Traits\HasDetails;

class SurveyAnswer extends Model
{
    use SoftDeletes;
    use HasDetails;

    protected $table = "survey_answers";
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends = ['result_count', 'result_rate'];

    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function question()
    {
        return $this->belongsTo(SurveyQuestion::class);
    }

    public function results()
    {
        return $this->hasMany(SurveyResultAnswer::class);
    }

    public function details()
    {
        return $this->hasMany(SurveyAnswerDetail::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function getResultCountAttribute()
    {
        return $this->results->count();
    }

    public function getResultRateAttribute()
    {
        $resultCount = $this->result_count;
        $totalCount = SurveyResultAnswer::where('survey_question_id', $this->survey_question_id)->get()->count();

        $multiplied = (100 * $resultCount);

        return  $multiplied ? $multiplied/ $totalCount : 0;
    }

}
