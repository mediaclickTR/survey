<?php
/**
 * Created by PhpStorm.
 * User: eraye
 * Date: 27.01.2019
 * Time: 04:48
 */

namespace Mediapress\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Auth\Models\User;

class SurveyLog extends Model
{
    protected $table = "survey_logs";
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];


    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }
}
