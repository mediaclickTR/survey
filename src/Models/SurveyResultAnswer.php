<?php
/**
 * Created by PhpStorm.
 * User: eraye
 * Date: 27.01.2019
 * Time: 04:48
 */

namespace Mediapress\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Auth\Models\User;

class SurveyResultAnswer extends Model
{
    use SoftDeletes;
    protected $table = "survey_result_answers";
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function result()
    {
        return $this->belongsTo(SurveyResult::class, 'survey_result_id', 'id');
    }

    public function question()
    {
        return $this->belongsTo(SurveyQuestion::class);
    }

    public function answer()
    {
        return $this->belongsTo(SurveyAnswer::class);
    }
}
