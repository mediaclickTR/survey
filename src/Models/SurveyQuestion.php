<?php
/**
 * Created by PhpStorm.
 * User: eraye
 * Date: 27.01.2019
 * Time: 04:48
 */

namespace Mediapress\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\Content\Traits\HasDetails;

class SurveyQuestion extends Model
{
    use SoftDeletes;
    use HasDetails;

    protected $table = "survey_questions";
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function survey()
    {
        return $this->belongsTo(Survey::class);
    }

    public function details()
    {
        return $this->hasMany(SurveyQuestionDetail::class);
    }

    public function answers()
    {
        return $this->hasMany(SurveyAnswer::class);
    }

    public function results()
    {
        return $this->hasMany(SurveyResultAnswer::class)->whereNull('survey_answer_id')->whereNotNull('custom_answer');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function children()
    {
        return $this->hasMany(SurveyQuestion::class);
    }

}
