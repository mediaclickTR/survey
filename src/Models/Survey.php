<?php
/**
 * Created by PhpStorm.
 * User: eraye
 * Date: 27.01.2019
 * Time: 04:48
 */

namespace Mediapress\Survey\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mediapress\Modules\Auth\Models\Admin;

class Survey extends Model
{
    use SoftDeletes;
    protected $table = "surveys";
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function questions()
    {
        return $this->hasMany(SurveyQuestion::class);
    }

    public function answers()
    {
        return $this->hasMany(SurveyAnswer::class);
    }

    public function results()
    {
        return $this->hasMany(SurveyResult::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
