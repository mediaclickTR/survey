<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('survey_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->integer('admin_id');
            $table->integer('survey_id');
            $table->integer('survey_question_id');
            $table->tinyInteger("status")->default(2);
            $table->integer("order");
            $table->string("cvar")->nullable();
            $table->text("ctex")->nullable();
            $table->integer("cint")->nullable();
            $table->integer("image")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_answers');
    }
}
