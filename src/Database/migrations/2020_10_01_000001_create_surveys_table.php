<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = \Illuminate\Support\Facades\DB::connection()->getSchemaBuilder();
        $schema->blueprintResolver(function($table, $callback) { return new Blueprint($table, $callback); });
        $schema->create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("website_id")->unsigned();
            $table->integer("admin_id")->unsigned();
            $table->tinyInteger("status")->default(2);
            $table->string("name")->nullable();
            $table->string("key");
            $table->string('cookie_id');
            $table->date("start_date")->nullable();
            $table->date("finish_date")->nullable();
            $table->tinyInteger("result_time")->default(2);
            $table->tinyInteger("voting_condition")->default(2);
            $table->tinyInteger("same_ip")->default(2);
            $table->tinyInteger("same_cookie")->default(2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}
