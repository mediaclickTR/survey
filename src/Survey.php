<?php

namespace Mediapress\Survey;


use Mediapress\Models\MPModule;

class Survey extends MPModule
{
    public $name = "Survey";
    public $url = "mp-admin/Survey";
    public $description = "Survey";
    public $author = "";
    public $menus = [];
    public $plugins = [];
}
